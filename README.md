## New wallet
`docker run -it --rm -v wallet:/home/lthn/wallet lthn/wallet new-wallet`

## Open Wallet
`docker run -it --rm -v wallet:/home/lthn/wallet lthn/wallet`

## Open Wallet RPC
`docker run -it --rm -v wallet:/home/lthn/wallet lthn/wallet wallet-rpc`

## Environment Variables

```shell
PORT_RPC=48782
WALLET=lethean.wallet
DAEMON_HOST=seed.lethean.io
```

## Use Private Daemon
`docker run -it --rm -e DAEMON_HOST=127.0.0.1 -v wallet:/home/lthn/wallet lthn/wallet`