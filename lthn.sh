#!/usr/bin/env bash

function newWallet() {
  if ! [ -f "${WALLET:-lethean.wallet}" ]; then
    echo "Generating wallet ${WALLET:-lethean.wallet}"
    lethean-wallet-cli --mnemonic-language English --generate-new-wallet "${WALLET:-lethean.wallet}" --trusted-daemon --daemon-host "${DAEMON_HOST:-seed.lethean.io}" exit
  fi
}

case $1 in
new-wallet)
  newWallet
  ;;

wallet-rpc)
    if ! [ -f "${WALLET:-lethean.wallet}" ]; then
      newWallet
      fi
  lethean-wallet-rpc --wallet-file "${WALLET:-lethean.wallet}" --rpc-bind-port "${PORT_RPC:-48782}" --daemon-host "${DAEMON_HOST:-seed.lethean.io}" --trusted-daemon
  ;;
*)
  if ! [ -f "${WALLET:-lethean.wallet}" ]; then
    newWallet
    fi
  lethean-wallet-cli --wallet-file "${WALLET:-lethean.wallet}" --daemon-host "${DAEMON_HOST:-seed.lethean.io}" --trusted-daemon
  ;;
esac

